## Source applicatives

Gérer le code source de l'application dans le répertoire `src/` à la racine.

## Environnement

### URL de l'API

  - http://localhost:8080/api

### Gestion de la pile Docker

- Démarrage / initialisation : `docker-compose up`
- Arrêt : `docker-compose stop`
- Destruction : `docker-compose down`


### Création de la base et mise à jour
Automatique lors du docker-compose up
```shell
php bin/console doctrine:schema:update --force # force la mise à jour
```

## Import manuel des données

```bash
-- Ajout d'utilisateur --
php bin/console doctrine:fixtures:load --group=users --append
```

### Swagger
- Url d'accès au swagger : http://localhost:8080/api
```bash
-- Generate swagger.json file --
`php bin/console api:openapi:export --spec-version=3 --output=/var/www/html/swagger.json
```

### Jouer les tests unitaires

```bash
./bin/phpunit tests/
```
