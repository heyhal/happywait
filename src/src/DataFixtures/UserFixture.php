<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixture extends Fixture implements FixtureGroupInterface
{
    private UserPasswordEncoderInterface $encoder;
    private string $password;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
        $this->password = $encoder->encodePassword(new User(), "H@ppyWait1");
    }

    public static function getGroups(): array
    {
        return ['users'];
    }

    public function load($manager)
    {
        $users = [
            ["lastName" => "Scylla", "firstName" => "Eddy", "userName" => "Ed_Scy"],
            ["lastName" => "Hochet", "firstName" => "Jean-Frédéric", "userName" => "JF_Hoc"],
            ["lastName" => "Hole", "firstName" => "Lucie", "userName" => "Luciolle"],
            ["lastName" => "Abbé", "firstName" => "Oscar", "userName" => "Scarabé"],
            ["lastName" => "Égérie", "firstName" => "Tom", "userName" => "T_Jerry"]
        ];
        foreach ($users as $u) {
            $user = new User();
            $user->setLastName($u['lastName']);
            $user->setFirstName($u['firstName']);
            $email = $user->getFirstName() . '.' . $user->getLastName() . '@domain.com';
            $user->setUsername($u['userName']);
            $user->setPassword($this->password);
            $user->setEmail($email);
            $manager->persist($user);
        }
        $manager->flush();
    }
}
