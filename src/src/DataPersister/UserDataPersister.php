<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\Channel;
use App\Entity\Message;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

class UserDataPersister implements ContextAwareDataPersisterInterface
{
    private $userPasswordEncoder;
    private $decorated;
    private $security;

    public function __construct(Security $security,
                                UserPasswordEncoderInterface $userPasswordEncoder,
                                ContextAwareDataPersisterInterface $decorated)
    {
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->decorated = $decorated;
        $this->security = $security;
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof User || $data instanceof Message || $data instanceof Channel;
    }

    public function persist($data, array $context = [])
    {
        $user = $this->security->getUser();
        if($data instanceof User) {
            if ($data->getPlainPassword()) {
                $data->setPassword(
                    $this->userPasswordEncoder->encodePassword($data, $data->getPlainPassword())
                );
                $data->eraseCredentials();
            }
        } else if ($data instanceof Message && $user instanceof User){
            $data->setCreatedBy($user);
        } else if ($data instanceof Channel && $user instanceof User){
            $data->addUser($user);
        }

        return $this->decorated->persist($data, $context);
    }

    public function remove($data, array $context = [])
    {
        return $this->decorated->remove($data, $context);
    }
}
