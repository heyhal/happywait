<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ApiResource(
 *      itemOperations={
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={
 *                  "groups"={
 *                      Channel::READ,
 *                      Message::ID
 *                  }
 *              }
 *          }
 *      },
 *      collectionOperations={
 *          "post"={
 *              "method"="POST",
 *              "denormalization_context"={
 *                  "groups"={
 *                      Channel::POST,
 *                      User::ID
 *                  }
 *              },
 *              "normalization_context"={
 *                  "groups"={
 *                      Channel::LIST
 *                  }
 *              }
 *          },
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={
 *                  "groups"={
 *                      Channel::LIST
 *                  }
 *              }
 *          }
 *     }
 * )
 */
class Channel
{
    public const READ = 'Channel.READ';
    public const POST = 'Channel.POST';
    public const LIST = 'Channel.LIST';
    public const ID = 'Channel.ID';
    /**
     * @var ?int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    public ?int $id = null;
    /**
     * @var ?string
     * @ORM\Column(type="string", length=100, unique=true, nullable=true)
     * @Groups({
     *     Channel::POST,
     *     Channel::LIST
     * })
     */
    private ?string $name;
    /**
     * @var Collection
     * @OneToMany(targetEntity="App\Entity\Message", mappedBy="channel")
     * @Groups({
     *     Channel::READ
     * })
     */
    private Collection $messages;
    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="channels")
     * @Assert\Count(min = "1")
     * @Groups({
     *     Channel::POST
     * }),
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"= {
     *             "type"="array",
     *              "items" = {"type"="object", "properties" = {"id"= {"type" = "integer"}}},
     *             "properties" = {"id"= {"type" = "integer"}}
     *         }
     *     }
     * )
     */
    private Collection $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->messages = new ArrayCollection();
    }

    /**
     * @return Collection
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    /**
     * @param Message $message
     * @return $this
     */
    public function addMessage(Message $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
        }

        return $this;
    }

    /**
     * @param Message $message
     * @return $this
     */
    public function removeMessage(Message $message): self
    {
        if ($this->messages->contains($message)) {
            $this->messages->removeElement($message);
        }

        return $this;
    }

    /**
     * @return Collection
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    /**
     * @param ArrayCollection $users
     */
    public function setUsers(ArrayCollection $users)
    {
        $this->users = $users;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[]= $user;
            $user->addChannel($this); // synchronously updating inverse sid
        }

        return $this;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Channel
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getCountMessage(){
        return $this->messages->count();
    }
}
