<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ApiResource(
 *      attributes={"order"={"createdBy"="desc"}},
 *      itemOperations={
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={
 *                  "groups"={
 *                      Message::LIST,
 *                      User::USERNAME
 *                  }
 *              }
 *          }
 *     },
 *      collectionOperations={
 *          "post"={
 *              "method"="POST",
 *              "denormalization_context"={
 *                  "groups"={
 *                      Message::POST,
 *                      Channel::POST,
 *                      Channel::ID
 *                  }
 *              },
 *             "normalization_context"={
 *                  "groups"={
 *                      Message::LIST,
 *                      User::ID,
 *                      User::USERNAME
 *                  }
 *              }
 *          },
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={
 *                  "groups"={
 *                      Message::LIST,
 *                      User::ID,
 *                      User::USERNAME
 *                  }
 *              }
 *          }
 *     }
 * )
 * @ApiFilter(SearchFilter::class, properties={
 *     "channel.id": "exact"
 * })
 */
class Message
{
    public const LIST = 'Message.LIST';
    public const POST = 'Message.POST';
    public const ID = 'Message.ID';
    /**
     *
     * @var ?int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     * @Groups({
     *     Message::LIST,
     *     Message::ID
     * })
     */
    public ?int $id = null;
    /**
     * @var Channel
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="App\Entity\Channel", inversedBy="messages", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({
     *     Message::POST
     * })
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="object",
     *             "properties" = {"id"= {"type" = "integer"}}
     *         }
     *     }
     * )
     */
    private Channel $channel;
    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     * @Groups({
     *     Message::LIST
     * })
     */
    private DateTime $createdAt;
    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({
     *     Message::LIST
     * })
     */
    private User $createdBy;
    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=5000)
     * @Groups({
     *     Message::LIST,
     *     Message::POST
     * })
     */
    private string $content;

    public function __construct()
    {
        $this->createdAt = new DateTime();
    }

    /**
     * @return Channel
     */
    public function getChannel(): Channel
    {
        return $this->channel;
    }

    /**
     * @param Channel $channel
     * @return Message
     */
    public function setChannel(Channel $channel): self
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * @return User
     */
    public function getCreatedBy(): User
    {
        return $this->createdBy;
    }

    /**
     * @param User $user
     * @return Message
     */
    public function setCreatedBy(User $user): self
    {
        $this->createdBy = $user;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return Message
     */
    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }
}
