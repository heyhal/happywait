<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ApiResource(
 *      itemOperations={
 *          "get"={
 *              "method"="GET",
 *              "security"="object == user",
 *              "normalization_context"={
 *                  "groups"={
 *                      User::READ
 *                  }
 *              }
 *          },
 *          "put"={
 *              "method"="PUT",
 *              "security"="object == user",
 *              "denormalization_context"={
 *                  "groups"={
 *                      User::PUT
 *                  }
 *              },
 *             "normalization_context"={
 *                  "groups"={
 *                      User::READ
 *                  }
 *              }
 *          }
 *      },
 *      collectionOperations={
 *          "post"={
 *              "method"="POST",
 *              "denormalization_context"={
 *                  "groups"={
 *                      User::POST
 *                  }
 *              },
 *             "normalization_context"={
 *                  "groups"={
 *                      User::READ
 *                  }
 *              },
 *             "validation_groups" = {"Default", "postValidation"}
 *          },
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={
 *                  "groups"={
 *                      User::LIST
 *                  }
 *              }
 *          },
 *     }
 * )
 */
class User implements UserInterface, JsonSerializable
{
    public const READ = 'User.READ';
    public const LIST = 'User.LIST';
    public const PUT = 'User.PUT';
    public const POST = 'User.POST';
    public const USERNAME = 'User.USERNAME';
    public const ID = 'User.ID';
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     * @Groups({
     *     User::ID,
     *     User::READ,
     *     User::LIST,
     *     User::PUT
     * })
     */
    public ?int $id;
    /**
     * @var ?string
     * @ORM\Column(type="string", length=100, unique=true, nullable=false)
     * @Assert\NotBlank()
     * @Groups({
     *     User::READ,
     *     User::LIST,
     *     User::POST,
     *     User::USERNAME
     * })
     */
    private ?string $username;
    /**
     * @var ?string
     * @ORM\Column(type="string", length=50, nullable=false)
     * @Assert\NotBlank()
     * @Groups({
     *     User::READ,
     *     User::LIST,
     *     User::PUT,
     *     User::POST
     * })
     *
     */
    private ?string $lastName;
    /**
     * @var ?string
     * @ORM\Column(type="string", length=50, nullable=false)
     * @Assert\NotBlank()
     * @Groups({
     *     User::READ,
     *     User::LIST,
     *     User::PUT,
     *     User::POST
     * })
     *
     */
    private ?string $firstName;
    /**
     * @var ?string
     * @ORM\Column(type="string", length=100, unique=true)
     * @Assert\NotBlank()
     * @Assert\Email()
     * @Groups({
     *     User::READ,
     *     User::POST
     * })
     */
    private ?string $email;
    /**
     * @var ?string
     * @SerializedName("password")
     * @Assert\NotBlank(groups={"postValidation"})
     * @Assert\Regex(pattern="/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}$/i", groups={"postValidation"}))
     * @Groups({
     *     User::POST
     * })
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "example" = "H@ppyWait1"
     *         }
     *     }
     * )
     */
    private ?string $plainPassword;
    /**
     * @var string
     * @ORM\Column(type="string", length=100)
     */
    private string $password;
    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     */
    private DateTime $createdAt;

    /**
     * @ORM\ManyToMany(targetEntity="Channel", mappedBy="users")
     */
    private Collection $channels;

    public function __construct()
    {
        $this->createdAt = new DateTime();
        $this->channels = new ArrayCollection();
    }

    /**
     * Returns the password used to authenticate the user.
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     * @return string|null The encoded password if any
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return Collection
     */
    public function getChannels(): Collection
    {
        return $this->channels;
    }

    /**
     * @param Channel $channel
     * @return $this
     */
    public function addChannel(Channel $channel): self
    {
        if (!$this->channels->contains($channel)) {
            $this->channels[] = $channel;
        }

        return $this;
    }

    /**
     * @param $password
     * @return User
     */
    public function setPassword($password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param string|null $plainPassword
     * @return User
     */
    public function setPlainPassword(?string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     * This can return null if the password was not encoded using a salt.
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @return string The username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     * @return User
     */
    public function setUsername($username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     * @return User
     */
    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     * @return User
     */
    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return User
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getRoles()
    {
        return array('ROLE_USER');
    }

    public function getUserIdentifier()
    {
        return $this->username;
    }

    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
