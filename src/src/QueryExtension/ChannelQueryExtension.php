<?php

namespace App\QueryExtension;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Building;
use App\Entity\Cadastre;
use App\Entity\Channel;
use App\Entity\Lot;
use App\Entity\Message;
use App\Entity\Occupation;
use App\Entity\Site;
use App\Entity\User;
use App\Service\ConnectedUserAccountService;
use App\Service\PermissionService;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Security;

class ChannelQueryExtension implements QueryCollectionExtensionInterface, QueryItemExtensionInterface
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function applyToCollection(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    private function addWhere(QueryBuilder $queryBuilder, string $resourceClass): void
    {
        if (
            Message::class == $resourceClass
            || Channel::class == $resourceClass
        ) {
            $user = $this->security->getToken()->getUser();
            if (isset($user) and get_class($user) === User::class) {
                $userId = $user->id;
                $rootAlias = $queryBuilder->getRootAliases()[0];
                if (Message::class === $resourceClass) {
                    $queryBuilder->innerJoin(sprintf('%s.channel', $rootAlias), 'channel')
                        ->innerJoin(sprintf('%s.users', 'channel'), 'users')
                        ->andWhere('users.id = :id');
                } else {
                    $queryBuilder->innerJoin(sprintf('%s.users', $rootAlias), 'users')
                        ->andWhere('users.id = :id');
                }
                $queryBuilder->setParameter('id', $userId);
            } else {
                throw new AccessDeniedHttpException();
            }
        }
    }

    public function applyToItem(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, array $identifiers, string $operationName = null, array $context = [])
    {
        $this->addWhere($queryBuilder, $resourceClass);
    }
}
