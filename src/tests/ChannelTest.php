<?php

namespace App\Tests;

use App\Entity\Channel;
use App\Entity\Message;
use PHPUnit\Framework\TestCase;

class ChannelTest extends TestCase
{
    public function testCountMessage(): void
    {
        $channel = new Channel();
        $this->assertSame(0, $channel->getCountMessage(), 'Pas de message');

        $channel->addMessage(new Message());
        $this->assertSame(1, $channel->getCountMessage(), '1 message');
    }
}
